package com.app.mymvpapplication.ui.post

import com.app.mymvpapplication.R
import com.app.mymvpapplication.base.BasePresenter
import com.app.mymvpapplication.network.PostApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import retrofit2.HttpException
import javax.inject.Inject

/**
 * The Presenter that will present the Post view.
 * @param postView the Post view to be presented by the presenter
 * @property postApi the API interface implementation
 * @property subscription the subscription to the API call
 */

class PostPresenter(postView: PostView) : BasePresenter<PostView>(postView) {

    @Inject
    lateinit var postApi: PostApi

    private var subscription = CompositeDisposable()

    override fun onViewCreated() {
        loadPosts()
    }

    /**
     * Loads the posts from the API and presents them in the view when retrieved, or showss error if
     * any.
     */
    //RX
//    private fun loadPosts() {
//        view.showLoading()
//            postApi.getPostsAsync()
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribeOn(Schedulers.io())
//            .doOnTerminate { view.hideLoading() }.apply {
//                    subscribe(
//                        { postList -> view.updatePosts(postList) },
//                        { view.showError(R.string.unknow_error) }
//                    ).addTo(subscription)
//                }
//    }

    //Coroutine
    private fun loadPosts() {
        view.showLoading()

        CoroutineScope(Dispatchers.IO).launch {
            withContext(Dispatchers.Main) {
                try {
                    val response = postApi.getPostsAsync().await()
                    when (response.code()) {
                        200 -> {
                            response.body()?.let { view.updatePosts(it) }
                        }
                        else -> view.showError("Error : ${response.code()}")
                    }

                } catch (e: RuntimeException) {
                    view.showError(e.toString())
                } finally {
                    view.hideLoading()
                }
            }
        }
    }

    override fun onViewDestroyed() {
        subscription.clear()
    }
}