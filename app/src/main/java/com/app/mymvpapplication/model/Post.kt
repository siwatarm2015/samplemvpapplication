package com.app.mymvpapplication.model

data class Post(val userId: Int, val id: Int, val title: String, val body: String)