package com.app.mymvpapplication.network

import com.app.mymvpapplication.model.Post
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

/**
 * The interface which provides methods to get result of webservices
 */
interface PostApi {

    /**
     * Get the list of the pots from the API
     */
    @GET("/posts")
//    fun getPosts(): Observable<List<Post>>
    fun getPostsAsync(): Deferred<Response<List<Post>>>
}